import { Injectable } from '@angular/core';

import "rxjs/add/operator/toPromise";
import { AlertController, Platform } from '../../../node_modules/ionic-angular';
import { UsersService } from './users.service';

@Injectable()
export class MembershipService {
  clientId = localStorage.getItem('currentId');
  constructor(public alertCtrl: AlertController,
  public mainService: UsersService,
  public plt: Platform){
  }
  
  //CALCULAR FECHA DE MEMBRESIA
  calculateMembership() {
    //FECHA ACTUAL
    var date = new Date();
    var fechaActual = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    //FECHA DE MEMBRESIA
    var fechaVigenciaMembresia = new Date(localStorage.getItem('currentMembresiaFin'));
    var fecharCorteMembresia = new Date(localStorage.getItem('currentMembresiaFin'));
    var fechaFinMembresia = new Date(localStorage.getItem('currentMembresiaFin')),
    //FECHA DE VIGENCIA
    addTime = -2 * 86400;
    fechaVigenciaMembresia.setSeconds(addTime);
    var fechaVigencia = new Date(fechaVigenciaMembresia.getFullYear(), fechaVigenciaMembresia.getMonth(), fechaVigenciaMembresia.getDate(), 0, 0, 0, 0);
    //FECHA DE CORTE
    addTime = 4 * 86400;
    fecharCorteMembresia.setSeconds(addTime);
    var fechaCorte = new Date(fecharCorteMembresia.getFullYear(), fecharCorteMembresia.getMonth(), fecharCorteMembresia.getDate(), 0, 0, 0, 0);
    var fechaFin = new Date(fechaFinMembresia.getFullYear(), fechaFinMembresia.getMonth(), fechaFinMembresia.getDate(), 0, 0, 0, 0);
    //TIEMPO DE VIGENCIA
    let tiempoVigencia = Math.floor(fechaVigencia.getTime()/100000);
    let tiempoCorte = Math.floor(fechaCorte.getTime()/100000);
    let tiempoHoy = Math.floor(fechaActual.getTime()/100000);
    let tiempoFin = Math.floor(fechaFin.getTime()/100000);
    
      if(localStorage.getItem('currentMembresiaFin')) {
        if(localStorage.getItem('currentMembresiaFin') != 'null') {
          if(tiempoHoy < tiempoVigencia) {
            console.log("MEMBRESIA ACTIVA :D")
          } else {
            if((tiempoVigencia <= tiempoHoy) && tiempoHoy <= tiempoCorte) {
              if((tiempoVigencia <= tiempoHoy) && tiempoHoy <= tiempoFin) {
                this.confirmation('Tiempo de Vigencia', 'Quedan menos de 5 días para disfrutar de los beneficios de ZFIT.');
              } else if((tiempoHoy <= tiempoCorte) && tiempoHoy >= tiempoFin) {
                this.confirmation('Fecha de Corte', 'Necesita renovar su membresía para seguir disfrutando de los beneficios de ZFIT.');
              }
            } else {
              this.confirmation('Fin de Membresía', 'Necesita renovar su membresía para seguir disfrutando de los beneficios de ZFIT.');
              this.updated();
            }
          }
        
      }
    }
  }

  //ACTUALIZAR USUARIO
  public updated() {
    let data = {
      inicioMembresia: '',
      finMembresia: '',
      tipoNivel: 0,
      opcion14: 0,
      opcion13: 0,
      id: this.clientId
    }
    this.mainService.update(data)
    .then(response => {
      localStorage.setItem('currentMembresiaFin', response.finMembresia);
      localStorage.setItem('currentMembresia', response.membresia);
    }).catch(error => {
      console.log(error);
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

}