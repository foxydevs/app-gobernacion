import { Injectable } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationResponse, BackgroundGeolocationConfig } from '@ionic-native/background-geolocation';
import "rxjs/add/operator/toPromise";

@Injectable()
export class GeolocationService {
    logs: string[] = [];
  constructor(private backgroundGeolocation: BackgroundGeolocation) {
  }

  start(){

    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 1,
      distanceFilter: 1,
      debug: true,
      stopOnTerminate: false,
      // Android only section
      locationProvider: 1,
      startForeground: true,
      interval: 180000,
      fastestInterval: 180000,
      activitiesInterval: 180000,
    };
  
    console.log('start');
  
    this.backgroundGeolocation
    .configure(config).subscribe((location: BackgroundGeolocationResponse) => {
      console.log(location);
      this.logs.push(`${location.latitude},${location.longitude}`);
    });
  
    // start recording location
    this.backgroundGeolocation.start();
  
  }

  startBackgroundGeolocation(){
    this.backgroundGeolocation.isLocationEnabled()
    .then((rta) =>{
      if(rta){
        this.start();
      }else {
        this.backgroundGeolocation.showLocationSettings();
      }
    })
  }

  stopBackgroundGeolocation(){
    this.backgroundGeolocation.stop();
  }

}