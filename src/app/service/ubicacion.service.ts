import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class UbicacionService {
	headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http:Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Todo
  public getAll():Promise<any> {
    let url = `${this.basePath}ubicaciones`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Eventos Por Usuario
  public getAllUser(id:any):Promise<any> {
    let url = `${this.basePath}ubicaciones/${id}/users`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Obtener Eventos Por Usuario
  public getAllClients(id:any):Promise<any> {
    let url = `${this.basePath}ubicaciones/${id}/clients`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Obtener Eventos Por Usuario
  public getAllType(id:any, estado:any):Promise<any> {
    let url = `${this.basePath}users/${id}/ubicaciones/${estado}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Crear Evento 
  public create(form):Promise<any> {
    let url = `${this.basePath}ubicaciones`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Eliminar Evento
  public delete(id):Promise<any> {
    let url = `${this.basePath}ubicaciones/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Evento
  public update(form):Promise<any> {
    let url = `${this.basePath}ubicaciones/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Evento
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}ubicaciones/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }
}