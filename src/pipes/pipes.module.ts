import { NgModule } from '@angular/core';
import { TimeAgoPipe } from './time-ago/time-ago';
import { SanitizerPipe } from './sanitizer/sanitizer';
import { Autosize } from './autozise';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    TimeAgoPipe,
    SanitizerPipe,
    Autosize
  ],
  imports: [
    IonicModule
  ],
  exports: [
    TimeAgoPipe,
    SanitizerPipe,
    Autosize
  ]
})
export class PipeModule {}