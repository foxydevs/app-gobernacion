import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';

//IMPORTAR SERVICIOS
import { AuthService } from '../../app/service/auth.service';
import { UsersService } from '../../app/service/users.service';
import { path } from "./../../app/config.module";
import { ModalController } from '../../../node_modules/ionic-angular/components/modal/modal-controller';
import { StaffService } from '../../app/service/staff.service';
import { AlertController } from '../../../node_modules/ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  //PROPIEDADES
  public users:any[] = [];
  public staff:any[] = [];
  public nacionality: Array<{name:string}>;
  public btnDisabled:boolean;
  public btnDisabled2:boolean;
  public baseId:number = path.id;
  //public baseUser:any;
  public pictureLogin:string;
  public user:any = {
    username: '',
    email: '',
    password: '',
    password_repeat: '',
    usuario: this.baseId,
    accept: false,
    firstname: '',
    lastname: '',
    work: '',
    description: '',
    referencia: 0,
    refer: 0,
    age: '',
    birthday: '',
    search: '',
    opcion18: '',
    phone: ''
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public modal: ModalController,
    public authentication: AuthService,
    public usersService: UsersService,
    public staffService: StaffService,
    public alertCtrl: AlertController 
    ) {
    this.pictureLogin = localStorage.getItem('currentPictureLogin')
    this.loadAllUsers();
    this.getAll();
    this.getNacionality();
  }

  //BUSCAR CÓDIGO
  public search() {
    if(this.user.search.length == 36) {
      console.log(this.user.search)
      this.usersService.findCommerce(this.user.search)
      .then(response => {
        console.log(response)
        if(response) {
          this.user.opcion18 = this.user.search;
          this.btnDisabled2 = true;
          this.confirmation('Código Encontrado', 'El código fue ingresado exitosamente, puede continuar con el registro.');
        } else {
          this.confirmation('Código No Encontrado', 'El código que ingreso no pertenece a ninguna empresa, verifique que lo haya escrito correctamente.');
          this.btnDisabled2 = false;
        }
      }).catch(error => {
        this.confirmation('Código No Encontrado', 'El código que ingreso no pertenece a ninguna empresa, verifique que lo haya escrito correctamente.');
        this.btnDisabled2 = false;
      })
    }    
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear();
    })
  }

  //CARGAR USUARIOS
  public getAll(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.staffService.getAllUser(path.id)
    .then(response => {
      this.staff = [];
      this.staff = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CREAR CUENTA
  createAccount() {
    if(this.user.referencia == 0) {
      this.user.referencia = null;
    }
    if(this.user.username) {
      if(this.user.email) {
        if(this.user.password) {
          if(this.user.password_repeat) {
            if(this.user.password == this.user.password_repeat) {
              this.btnDisabled = true;
              this.create(this.user);
            } else {
              this.message('Las contraseñas deben ser iguales.')
            }
          } else {
            this.message('Confirmar contraseña es requerido.')
          }
        } else {
          this.message('La contraseña es requerida.')
        }
      } else {
        this.message('El correo electrónico es requerido.')
      }
    } else {
      this.message('El nombre de usuario es requerido.')
    }
  }

  openModal() {
    let chooseModal = this.modal.create('TermsAndConditionsPage');
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        if(data == 'Accept') {
          this.user.accept = true;
        }
      }      
    });
    chooseModal.present();
  }

  openModal2() {
    let chooseModal = this.modal.create('ModalStaffRegisterPage');
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        console.log(data);
        this.user.referencia = data.user;
      }
    });
    chooseModal.present();
  }

  //MINUSCULAS
  inputLowerCase() {
    this.user.username = this.user.username.toLowerCase().replace(" ", "");
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //AGREGAR
  create(formValue:any) {
    let load = this.loading.create({
      content: "Registrando..."
    });
    load.present();
    this.usersService.create(formValue)
    .then(response => {
      console.log(response)
      load.dismiss();
      this.navCtrl.setRoot('LoginPage');
    }).catch(error => {
      console.log(error.status)
      if(error.status == '400') {
        this.message('El nombre de usuario o correo electrónico ya existe.')
      }
      load.dismiss();
      this.btnDisabled = false;
      console.clear
    });
  }

  //NACIONALIDADES
  public getNacionality() {
    this.nacionality = [{name: 'No Aplica'},
    {name: 'Antigua y Barbuda'},
    {name: 'Argentina'},
    {name: 'Bahamas'},
    {name: 'Barbados'},
    {name: 'Belice'},
    {name: 'Bolivia'},
    {name: 'Brasil'},
    {name: 'Canadá'},
    {name: 'Barbados'},
    {name: 'Chile'},
    {name: 'Colombia'},
    {name: 'Costa Rica'},
    {name: 'Cuba'},
    {name: 'Dominica'},
    {name: 'Ecuador'},
    {name: 'El Salvador'},
    {name: 'Estados Unidos'},
    {name: 'Granada'},
    {name: 'Guatemala'},
    {name: 'Guyana'},
    {name: 'Haití'},
    {name: 'Honduras'},
    {name: 'Jamaica'},
    {name: 'México'},
    {name: 'Nicaragua'},
    {name: 'Panamá'},
    {name: 'Paraguay'},
    {name: 'Perú'},
    {name: 'República Dominicana'},
    {name: 'San Cristóbal y Nieves'},
    {name: 'San Vicente y las Granadinas'},
    {name: 'Santa Lucía'},
    {name: 'Surinam'},
    {name: 'Trinidad y Tobago'},
    {name: 'Uruguay'},
    {name: 'Venezuela'}]
  }
}
