import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DocumentsUserPage } from './documents-user';
 
@NgModule({
  declarations: [
    DocumentsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(DocumentsUserPage),
  ],
  exports: [
    DocumentsUserPage
  ]
})
export class DocumentsUserPageModule {}