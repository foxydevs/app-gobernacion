import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PromotionService } from '../../../app/service/promotion.service';
import { path } from '../../../app/config.module';

@IonicPage()
@Component({
  selector: 'promotion-user',
  templateUrl: 'promotion-user.html',
})
export class PromotionUserPage {
  //PROPIEDADES
  table:any[] = [];
  idUser:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'beneficios';

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: PromotionService,
  ) {}

  openForm(parameter?:any) {
    this.navCtrl.push('FormPromotionUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
