import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { PromotionService } from '../../../../app/service/promotion.service';
import { ProductsService } from '../../../../app/service/products.service';
import { EventsService } from '../../../../app/service/events.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-promotion-user',
  templateUrl: 'form-promotion-user.html',
})
export class FormPromotionUserPage {
  //PROPIEDADES
  private parameter:any;
  private title:any;
  private disabledBtn:boolean;
  private table:any[] = [];
  private table2:any[] = [];
  private basePath:string = path.path;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  private data = {
    title: '',
    description: '',
    inicio: '',
    fin: '',
    descuento: '',
    product: 'null',
    event: 'null',
    state: 0,
    user_created: path.id,
    picture: localStorage.getItem('currentPicture'),
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: PromotionService,
    public secondService: ProductsService,
    public thirdService: EventsService,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getAll(path.id);
    this.getAllThird(path.id);
    if(this.parameter) {
      this.title = "Edición Promoción";
      this.getSingle(this.parameter);  
    } else {
      this.title = "Agregar Promoción";
    }
  }

  //CARGAR BENEFICIOS
  public getAll(id:any) {
    this.secondService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR BENEFICIOS
  public getAllThird(id:any) {
    this.thirdService.getAllUser(id)
    .then(response => {
      this.table2 = []
      this.table2 = response;
    }).catch(error => {
      console.clear
    })
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    console.log(this.data)
    if(this.data.product == 'null') {
      this.data.product = null;
    }
    if(this.data.event == 'null') {
      this.data.event = null;
    }
    console.log(this.data)

    this.data.picture = $('img[alt="Avatar"]').attr('src');
    if(this.data.title) {
      if(this.data.description) {
        this.disabledBtn = true;
        if(this.parameter) {
          console.log(this.data)
          this.update(this.data)
        } else {
          console.log(this.data)
          this.create(this.data)
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El título es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Promoción Agregada', 'La promoción fue agregada exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Promoción Actualizada', 'La promoción fue actualizada exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el workout?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

//IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'workouts'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
}
