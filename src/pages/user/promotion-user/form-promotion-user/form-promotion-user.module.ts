import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormPromotionUserPage } from './form-promotion-user';
import { PipeModule } from '../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    FormPromotionUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormPromotionUserPage),
    PipeModule
  ],
  exports: [
    FormPromotionUserPage
  ]
})
export class FormPromotionUserPageModule {}