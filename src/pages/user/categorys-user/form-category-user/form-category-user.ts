import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from '../../../../app/config.module';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { File } from '@ionic-native/file';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-category-user',
  templateUrl: 'form-category-user.html'
})
export class CategoryFormUserPage implements OnInit {
  public categorie = {
    name: '',
    description : '',
    user_created: '',
    estado: 1,
    state: 1,
    picture: localStorage.getItem('currentPicture'),
    //picture2: localStorage.getItem('currentPicture'),
    id: ''
  }
  public parameter:any;
  public title;
  public disabledBtn:boolean = false;
  public basePath:string = path.path
  public pictureNew:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public camera: Camera, 
    public transfer: FileTransfer, 
    public file: File,
    public androidPermissions: AndroidPermissions
  ) {
    this.parameter = this.navParams.get('parameter');
    if(this.parameter != 'new') {
      this.getSingle(this.parameter);
      this.title = 'Edición Categoría';
    } else {
      this.categorie.user_created = localStorage.getItem("currentId");
      this.title = 'Nueva Categoría';
    }
  }

  ngOnInit() {
  }

  //Insertar Datos
  public saveChanges() {
    console.log(this.categorie)
    if(this.categorie.name) {
      if(this.categorie.description) {
        if(this.categorie.estado == 1) {
          this.disabledBtn = true;
          this.categorie.state = 1;
          if(this.parameter != 'new') {
            this.update(this.categorie);
          } else {
            this.create(this.categorie);
          }
        } else {
          this.categorie.state = 2;
          if(this.parameter != 'new') {
            this.update(this.categorie);
          } else {
            this.create(this.categorie);
          }
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El nombre es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Categoría Agregada', 'La categoría fue agregada exitosamente.');
      this.categorie.id = response.id;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Categoría Actualizada', 'La categoría fue actualizada exitosamente.');
      this.navCtrl.pop();
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.categorie.id = response.id;
      this.categorie.name = response.name;
      this.categorie.description = response.description;             
      this.categorie.picture = response.picture;
      this.categorie.user_created = response.user_created;
      if(response.state == 1) {
        this.categorie.estado = response.state;
      } else {
        this.categorie.estado = 0;        
      }
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.categorie.picture = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.categorie.picture = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }
  
  cropImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit:true,
      targetWidth:300,
      targetHeight:300
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.categorie.picture = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  uploadImage(){
    //Show loading
    let loader = this.loading.create({
      content: "Cargando..."
    });
    loader.present();

    //create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int
    var random = Math.floor(Math.random() * 100);

    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: "myImage_" + random + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: {}
    }

    //file transfer action
    fileTransfer.upload(this.categorie.picture, `${this.basePath}categorys/upload/${this.categorie.id}`, options)
      .then((data) => {
        alert("Success");
        loader.dismiss();
      }, (err) => {
        console.log(err);
        alert("Error");
        loader.dismiss();
      });
  }
  
  //IMAGEN DE CATEGORIA
  uploadImages(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}categorys/upload/${this.categorie.id}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        this.pictureNew = true;
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        //$('#imgAvatar').attr("src", respuesta.picture)
        //var confirmate = this.pictureNew = false;
        //https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif
        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  permission() {
    console.log('ACCEDIENDO')
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MANAGE_DOCUMENTS)
    .then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.MANAGE_DOCUMENTS)
    ).catch(error => {
      console.log(error)
    });
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.MANAGE_DOCUMENTS, this.androidPermissions.PERMISSION.MANAGE_DOCUMENTS]);
  }

}