import { Component } from '@angular/core';
import { NavController, IonicPage, ViewController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { UbicacionService } from '../../../../app/service/ubicacion.service';

//JQUERY
declare var google;

@IonicPage()
@Component({
  selector: 'form-blog-user',
  templateUrl: 'form-blog-user.html',
})
export class FormBlogUserPage {
  //PROPIEDADES
  public parameter:any;
  public map: any;
  public title:any;
  public data2:any;
  public basePath:string = path.path;
  public baseUserId = path.id;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    latitude: 0,
    longitude: 0
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public iab: InAppBrowser,
    public loading: LoadingController,
    public mainService: UbicacionService,
    public view: ViewController
  ) {
    this.parameter = this.navParams.get('parameter')
    this.getSingle(this.parameter);
    
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data.latitude = response.latitude;
      this.data.longitude = response.longitude;
      this.loadMapUpdate(this.data.latitude, this.data.longitude);
      this.data2 = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.data.latitude = latitude.toString();
  this.data.longitude = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitude = evt.latLng.lat();
      this.data.longitude = evt.latLng.lng();
    });
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }
}
