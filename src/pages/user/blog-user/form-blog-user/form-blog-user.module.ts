import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormBlogUserPage } from './form-blog-user';
import { PipeModule } from '../../../../pipes/pipes.module';
import { UbicacionService } from '../../../../app/service/ubicacion.service';
 
@NgModule({
  declarations: [
    FormBlogUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormBlogUserPage),
    PipeModule
  ],
  exports: [
    FormBlogUserPage
  ],
  providers: [
    UbicacionService
  ]
})
export class FormBlogUserPageModule {}