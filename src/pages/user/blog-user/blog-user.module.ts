import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogUserPage } from './blog-user';
import { UbicacionService } from '../../../app/service/ubicacion.service';
 
@NgModule({
  declarations: [
    BlogUserPage,
  ],
  imports: [
    IonicPageModule.forChild(BlogUserPage),
  ],
  exports: [
    BlogUserPage
  ],
  providers: [
    UbicacionService
  ]
})
export class BlogUserPageModule {}