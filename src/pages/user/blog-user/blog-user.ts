import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage, ModalController} from 'ionic-angular';
import { path } from '../../../app/config.module';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { UbicacionService } from '../../../app/service/ubicacion.service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'blog-user',
  templateUrl: 'blog-user.html',
})
export class BlogUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public months:any[] = [];
  public userID:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  logs: string[] = [];

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: UbicacionService,
    private backgroundGeolocation: BackgroundGeolocation,
    private modalController: ModalController,
    private alertCtrl: AlertController
  ) {
  }

  startBackgroundGeolocation(){
    console.log('NEL PRRO');
    this.backgroundGeolocation.isLocationEnabled()
    .then((rta) =>{
      if(rta){
        this.start();
      }else {
        this.backgroundGeolocation.showLocationSettings();
        console.log('NEL PRRO')
      }
    })
  }

  start(){

    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 1,
      distanceFilter: 1,
      debug: true,
      stopOnTerminate: false,
      // Android only section
      locationProvider: 1,
      startForeground: true,
      interval: 180000,
      fastestInterval: 180000,
      activitiesInterval: 180000
    };

    console.log('start');

    this.backgroundGeolocation
    .configure(config)
    .subscribe((location: BackgroundGeolocationResponse) => {
      console.log(location);
      this.logs.push(`${location.latitude},${location.longitude}`);
    });

    // start recording location
    this.backgroundGeolocation.start();

  }

  stopBackgroundGeolocation(){
    this.backgroundGeolocation.stop();
  }

  openForm(parameter?:any) {
    let chooseModal = this.modalController.create('FormBlogUserPage', { parameter });
    chooseModal.present();
  }

  //CARGAR
  getAll()  {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      console.log(response)
      this.table = []
      this.table = response;
      this.table.reverse();
      load.dismiss();
    }).catch(error => {
      console.clear
      load.dismiss();
    })
  }

  //CARGAR LOS RETOS
  public getAll2() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.table = []
      for(let x of response) {
        let data = {
          picture: x.picture,
          title: x.title,
          description: x.description,
          created_at: this.returnDate(x.created_at),
          id: x.id
        }
        this.table.push(data);
      }
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:any) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth() + 1)] + ', ' + date.getFullYear();
    return created_at;
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }


  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: 'Eliminar Alerta',
      message: '¿Deseas eliminar la alerta?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              console.clear();
              this.getAll();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }
}
