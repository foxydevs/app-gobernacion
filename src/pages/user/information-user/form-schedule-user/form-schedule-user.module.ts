import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormScheduleUserPage } from './form-schedule-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormScheduleUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormScheduleUserPage),
    PipeModule
  ],
  exports: [
    FormScheduleUserPage
  ]
})
export class FormScheduleUserPageModule {}