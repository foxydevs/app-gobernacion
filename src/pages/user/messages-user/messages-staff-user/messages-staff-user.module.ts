import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesStaffUserPage } from './messages-staff-user';
 
@NgModule({
  declarations: [
    MessagesStaffUserPage,
  ],
  imports: [
    IonicPageModule.forChild(MessagesStaffUserPage),
  ],
  exports: [
    MessagesStaffUserPage
  ]
})
export class MessagesStaffUserPageModule {}