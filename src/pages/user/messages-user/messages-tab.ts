import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { path } from '../../../app/config.module';

@IonicPage()
@Component({
  templateUrl: 'messages-tab.html'
})
export class MessagesTabPage {
  idUser:any = path.id;

  tab1Root = 'MessagesUserPage';
  tab2Root = 'MessagesContactsUserPage';
  tab3Root = 'MessagesStaffUserPage';

  constructor() {

  }
}