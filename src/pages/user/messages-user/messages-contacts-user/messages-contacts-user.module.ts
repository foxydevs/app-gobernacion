import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesContactsUserPage } from './messages-contacts-user';
 
@NgModule({
  declarations: [
    MessagesContactsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(MessagesContactsUserPage),
  ],
  exports: [
    MessagesContactsUserPage
  ]
})
export class MessagesContactsUserPageModule {}