import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendMessagesClientUserPage } from './messages-client-send-user';
 
@NgModule({
  declarations: [
    SendMessagesClientUserPage,
  ],
  imports: [
    IonicPageModule.forChild(SendMessagesClientUserPage),
  ],
  exports: [
    SendMessagesClientUserPage
  ]
})
export class SendMessagesClientUserPageModule {}