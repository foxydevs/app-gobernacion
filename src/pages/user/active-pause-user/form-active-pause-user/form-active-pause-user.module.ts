import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormActivePauseUserPage } from './form-active-pause-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormActivePauseUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormActivePauseUserPage),
    PipeModule
  ],
  exports: [
    FormActivePauseUserPage
  ]
})
export class FormActivePauseUserPageModule {}