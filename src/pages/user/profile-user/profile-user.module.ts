import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileUserPage } from './profile-user';
import { BarcodeScanner } from '../../../../node_modules/@ionic-native/barcode-scanner';
import { EncriptationService } from '../../../app/service/encriptation.service';
 
@NgModule({
  declarations: [
    ProfileUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileUserPage),
  ],
  exports: [
    ProfileUserPage
  ],
  providers: [
    BarcodeScanner,
    EncriptationService
  ]
})
export class ProfileUserPageModule {}