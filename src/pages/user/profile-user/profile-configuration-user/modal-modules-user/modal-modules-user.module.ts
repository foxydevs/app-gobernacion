import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalModulesUserPage } from './modal-modules-user';
 
@NgModule({
  declarations: [
    ModalModulesUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalModulesUserPage),
  ],
  exports: [
    ModalModulesUserPage
  ]
})
export class ModalModulesUserPageModule {}