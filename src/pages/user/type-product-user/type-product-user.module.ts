import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeProductUserPage } from './type-product-user';
import { PipeModule } from '../../../pipes/pipes.module';
import { PresentationService } from '../../../app/service/presentation.serive';
 
@NgModule({
  declarations: [
    TypeProductUserPage,
  ],
  imports: [
    IonicPageModule.forChild(TypeProductUserPage),
    PipeModule
  ],
  exports: [
    TypeProductUserPage
  ],
  providers: [
    PresentationService
  ]
})
export class TypeProductUserPageModule {}