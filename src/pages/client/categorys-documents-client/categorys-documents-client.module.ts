import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorysDocumentsClientPage } from './categorys-documents-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    CategorysDocumentsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CategorysDocumentsClientPage),
    PipeModule
  ],
  exports: [
    CategorysDocumentsClientPage
  ]
})
export class CategorysDocumentsClientPageModule {}