import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { CategorysService } from '../../../app/service/categorys.service';
import { path } from "./../../../app/config.module";
import { UsersService } from '../../../app/service/users.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { MembershipService } from '../../../app/service/membership.service';

@IonicPage()
@Component({
  selector: 'categorys-documents-client',
  templateUrl: 'categorys-documents-client.html'
})
export class CategorysDocumentsClientPage {
  //PROPIEDADES
  public categorys:any[] = [];
  public baseId:number = path.id;
  public pictureCategories:string = "http://foxylabs.xyz/Documentos/imgs/Category.jpg";
  public idClient:any = localStorage.getItem('currentId');
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  finMembresia = localStorage.getItem('currentMembresiaFin');

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public modal:ModalController,
    public mainService: MembershipService
  ) {
    this.idClient = localStorage.getItem('currentId');
    if(localStorage.getItem('currentPictureCategories')){
      this.pictureCategories = localStorage.getItem('currentPictureCategories');
    }
    if(localStorage.getItem('currentState') == '21') {
      this.openModalCreate();
    }
    this.loadSingleUser(this.baseId);
    this.loadAll();
    this.mainService.calculateMembership();
  }

  //Cargar los productos
  public loadAll(){
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    if((this.baseId+'')!='null'){
      this.categorysService.getAllUser(this.baseId)
      .then(response => {
        this.categorys = response;
        load.dismiss();
      }).catch(error => {
        // console.clear;
      })
    }
    else{
      this.categorysService.getAll()
      .then(response => {
        this.categorys = response;
        load.dismiss();
      }).catch(error => {
        // console.clear;
      })
    }
  }

  public loadSingleUser(id:number) {
    if((id+'')!='null'){
      this.usersService.getSingle(id)
      .then(response => {
        if(response.pic2){
          this.pictureCategories = response.pic2;
          localStorage.setItem('currentPictureCategories', response.pic2);
        }
        localStorage.setItem('currentPictureLogin', response.pic1);
        localStorage.setItem('currentpictureCover', response.pic3);

        //console.clear();
      }).catch(error => {
        // console.clear();
      })
    }else{
      localStorage.removeItem('currentPictureCategories');
      localStorage.removeItem('currentPictureLogin');
      localStorage.removeItem('currentpictureCover');
    }
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any) {
    this.navCtrl.push('DocumentsClientPage', { parameter });
  }

  //ABIR MODAL AGREGAR
  public openModalCreate() {
    let parameter = '2'
    let chooseModal = this.modal.create('ProfileConfigurationClientPage', { parameter });
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadSingleUser(this.baseId);
      this.loadAll();
      refresher.complete();
    }, 2000);
  }

}
