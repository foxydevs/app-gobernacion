import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileClientPage } from './profile-client';
import { PipeModule } from '../../../pipes/pipes.module';

@NgModule({
  declarations: [
    ProfileClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileClientPage),
    PipeModule,
  ],
  exports: [
    ProfileClientPage
  ]
})
export class ProfileClientPageModule {}