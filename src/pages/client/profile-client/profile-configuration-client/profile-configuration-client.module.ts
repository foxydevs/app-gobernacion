import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { ProfileConfigurationClientPage } from './profile-configuration-client';
import { AddressService } from '../../../../app/service/address.service';
 
@NgModule({
  declarations: [
    ProfileConfigurationClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileConfigurationClientPage),
    PipeModule
  ],
  exports: [
    ProfileConfigurationClientPage
  ],
  providers: [
    AddressService
  ]
})
export class ProfileConfigurationClientPageModule {}