import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { path } from '../../../app/config.module';
import { UbicacionService } from '../../../app/service/ubicacion.service';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

@IonicPage()
@Component({
  selector: 'blog-client',
  templateUrl: 'blog-client.html',
})
export class BlogClientPage {
  //PROPIEDADES
  public table:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  data = {
    fecha: '',
    hora: '',
    description: 'MENSAJE DE EMERGENCIA',
    latitude: 0, 
    longitude: 0,
    cliente: localStorage.getItem('currentId'),
    app: path.id,
    state: 0
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: UbicacionService,
    public secondService: UbicacionService,
    public geolocation: Geolocation,
    public alertCtrl: AlertController,
    public toast: ToastController
  ) {
  }

  //GET POSITION
  getPosition():any{
    this.geolocation.getCurrentPosition().then(resp => {
      this.getDateAndHour();
      this.data.latitude = resp.coords.latitude;
      this.data.longitude = resp.coords.longitude;
      this.create();
     }).catch(error => {
      console.log('Error getting location', error);
    });
  }

  //CARGAR FECHA Y HORA
  getDateAndHour() {
    let fecha = new Date();
    this.data.fecha = fecha.getFullYear() + '-' + fecha.getMonth() + '-' + fecha.getDate();
    this.data.hora = fecha.getHours() + ':' + fecha.getMinutes() + ':' + fecha.getSeconds();
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.getPosition();
  }

  //CREATE
  create() {
    this.mainService.create(this.data)
    .then(response => {
      this.confirmation('Alerta Enviada', 'Tu mensaje de alerta fue enviado exitosamente.')
      this.getAll()
    }).catch(error => {
      this.message(error);
      console.clear
    })
  }

  //CARGAR LOS RETOS
  public getAll() {
    this.mainService.getAll()
    .then(response => {
      this.table = [];
      this.table = response;
      this.table.reverse();
    }).catch(error => {
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

}
