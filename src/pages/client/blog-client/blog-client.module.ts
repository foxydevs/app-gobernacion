import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogClientPage } from './blog-client';
import { UbicacionService } from '../../../app/service/ubicacion.service';
 
@NgModule({
  declarations: [
    BlogClientPage,
  ],
  imports: [
    IonicPageModule.forChild(BlogClientPage),
  ],
  exports: [
    BlogClientPage
  ], 
  providers: [
    UbicacionService
  ]
})
export class BlogClientPageModule {}