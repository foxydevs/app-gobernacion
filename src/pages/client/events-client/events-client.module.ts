import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsClientPage } from './events-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    EventsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsClientPage),
    PipeModule
  ],
  exports: [
    EventsClientPage
  ]
})
export class EventsClientPageModule {}