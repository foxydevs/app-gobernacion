import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { path } from "./../../../app/config.module";
import { MembershipService } from '../../../app/service/membership.service';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'products-client',
  templateUrl: 'products-client.html'
})
export class ProductsClientPage {
  //Propiedades
  public products:any[] = [];
  public parameter:any;
  public baseId:number = path.id;
  public search:any;
  cartProducts:any[] = [];
  authentication:any = localStorage.getItem('currentAuthentication');
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  appMembresia = localStorage.getItem('currentAppMembresia');
  orders = localStorage.getItem('currentOrders');
  shopCart = localStorage.getItem('currentShopCart');

  //Constructor
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: MembershipService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.cartProducts = JSON.parse(localStorage.getItem('cart'));
    this.mainService.calculateMembership();
    if(this.parameter) {
      setTimeout(() => {
        this.loadAll(this.parameter);
      }, 500);
    } else {
      this.loadAllProducts()
    }
  }

  //AÑADIR AL CARRITO
  public saveChanges(product:any) {
    console.log(product)
    let data = {
      cantidad: 1,
      name: product.name,
      subtotal: product.price,
      price: product.price,
      precioVenta: product.price,
      precioClienteEs: null,
      precioDistribuidor: null, 
      producto: product.id,
      picture: product.picture,
      state: 4,
      comment: null
    }
    console.log(data)
    let cart:any[] = []
    cart = JSON.parse(localStorage.getItem('cart'))
    cart.push(data)
    localStorage.removeItem('cart')
    localStorage.setItem('cart', JSON.stringify(cart));
    this.message(data.name + ' agregado al carrito :D');
  }

  //COMPROBAR SI EXISTE EN CARRITO
  public comprobar(product:any) {
    for (var x in this.cartProducts) {
      if (this.cartProducts[x].producto == product.id) {
        let cant = parseInt(this.cartProducts[x].cantidad) + 1;
        let sub = cant * this.cartProducts[x].price;
        sub.toFixed(2);
        let data = {
          cantidad: cant,
          name: product.name,
          subtotal: sub,
          price: product.price,
          precioVenta: product.precioVenta,
          precioClienteEs: product.precioClienteEs,
          precioDistribuidor: product.precioDistribuidor, 
          producto: product.id,
          picture: product.picture
        }
        this.cartProducts[x] = data;
        console.log("EDICION", this.cartProducts[x])
        localStorage.setItem('cart', JSON.stringify(this.cartProducts));
        return true;
      }
    }
    return false;
  }

  //Cargar los Productos
  public loadAll(id:any){
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    if((this.baseId+'')!='null'){
      this.productsService.getAllUser(this.baseId)
      .then(response => {
        this.products = [];
        for(let x of response) {
          if(x.category == id) {
            this.products.push(x);
          }
        }
        load.dismiss();
      }).catch(error => {
        console.clear;
      })
    }else{
      this.productsService.getAll()
      .then(response => {
        for(let x of response) {
          if(x.category == id) {
            this.products.push(x);
          }
        }
        load.dismiss();
      }).catch(error => {
        console.clear;
      })
    }
  }

  public loadAllProducts(){
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.productsService.getAllUser(this.baseId)
    .then(response => {
      load.dismiss();
      this.products = [];
      this.products = response;
    }).catch(error => {
      console.clear;
    })
  }

  public makeAnOrder(parameter:any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      this.navCtrl.push('OrdersClientPage', { parameter });
    }
  }

  public seeMore(parameter:any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      this.navCtrl.push('SeeProductsClientPage', { parameter });
    }
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.parameter) {
        setTimeout(() => {
          this.loadAll(this.parameter);
        }, 500);
      } else {
        this.loadAllProducts()
      }
      refresher.complete();
    }, 2000);
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

  //OPEN CART
  openCart() {
    this.navCtrl.push('MyCartClientPage')
  }

}
