import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeProductsClientCommentPage } from './see-products-client-comment';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeProductsClientCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeProductsClientCommentPage),
    PipeModule
  ],
  exports: [
    SeeProductsClientCommentPage
  ]
})
export class SeeProductsClientCommentPageModule {}