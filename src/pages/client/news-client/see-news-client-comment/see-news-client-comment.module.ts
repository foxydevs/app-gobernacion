import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeNewsClientCommentPage } from './see-news-client-comment';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeNewsClientCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeNewsClientCommentPage),
    PipeModule
  ],
  exports: [
    SeeNewsClientCommentPage
  ]
})
export class SeeNewsClientCommentPageModule {}