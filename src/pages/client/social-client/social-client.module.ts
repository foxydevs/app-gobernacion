import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SocialClientPage } from './social-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SocialClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SocialClientPage),
    PipeModule
  ],
  exports: [
    SocialClientPage
  ]
})
export class SocialClientPageModule {}