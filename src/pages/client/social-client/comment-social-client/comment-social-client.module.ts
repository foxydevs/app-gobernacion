import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentSocialClientPage } from './comment-social-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    CommentSocialClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentSocialClientPage),
    PipeModule
  ],
  exports: [
    CommentSocialClientPage
  ]
})
export class CommentSocialClientPageModule {}