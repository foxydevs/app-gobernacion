import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { OrdersService } from '../../../../app/service/orders.service';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Platform } from 'ionic-angular/platform/platform';
import { File } from '@ionic-native/file';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'detail-orders-client',
  templateUrl: 'detail-orders-client.html'
})
export class DetailOrdersClientPage implements OnInit {
  //PROPIEDADES
  public parameter:any;
  public positions :any
  public saludar:any;
  public storageDirectory: string = '';
  public order = {
    name: '',
    description: '',
    unit_price: '',
    quantity: '',
    comment: '',
    aprobacion: '',
    fechaapro: '',
    user: '',
    deposito: '',
    picture: '',
    image: '',
    fecha: '',
    state: '',
    documento: '',
    libro: '',
    id: 0
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public orderService: OrdersService,
    public view: ViewController,
    public transfer: FileTransfer,
    public file: File,
    public platform: Platform,
    public alertCtrl: AlertController,
    public iab: InAppBrowser,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.saludar = this.parameter.ern;
    if(this.parameter) {
      if(this.parameter.type == 'detail') {
        this.findOrder(this.parameter.id)
      } else {
        alert(JSON.stringify(this.parameter))
        this.searchOrder(this.parameter.ern)
      }      
    } else {
      this.closeModal();
    }
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  ngOnInit() {
  }

  public searchOrder(ern:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.orderService.findSingle(ern)
    .then(res => {
      alert(JSON.stringify(res))
      this.orderService.transaction(res.id, this.parameter.tokenID)
      .then(res2 => {
        alert(JSON.stringify(res2))
        this.order.aprobacion = res2.aprobacion
        this.order.fecha = res2.fecha
      }).catch(error => {
        console.clear
        alert(error)
      })
        this.order.id = res.id;
        this.order.name = res.products.name;
        this.order.image = res.products.picture;
        this.order.description = res.products.description;
        this.order.quantity = res.quantity;
        this.order.unit_price = res.unit_price;
        this.order.comment = res.comment;
        this.order.deposito = res.deposito;
        this.order.fechaapro = res.fechaapro;
        this.order.state = res.state;
        this.order.libro = res.libro;
        this.order.user = res.users.firstname + ' ' + res.users.lastname;
        this.order.picture = res.picture;
        this.positions = '('+res.latitude+', '+res.longitude+')'
        load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  public findOrder(ern:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.orderService.getSingle(ern)
    .then(res => {
        this.order.id = res.id;
        this.order.name = res.products.name;
        this.order.image = res.products.picture;
        this.order.description = res.products.description;
        this.order.quantity = res.quantity;
        this.order.unit_price = res.unit_price;
        this.order.comment = res.comment;
        this.order.picture = res.picture;        
        this.order.deposito = res.deposito;
        this.order.aprobacion = res.aprobacion
        this.order.user = res.users.firstname + ' ' + res.users.lastname;
        this.order.fecha = res.fechaapro;
        this.order.state = res.state;
        this.order.libro = res.libro;
        this.positions = '('+res.latitude+', '+res.longitude+')'
        load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  public download() {
    //https://bpresentacion.s3.us-west-2.amazonaws.com/orders/
    const url = this.order.picture;

    //QUITAR RUTA DE CARPETA
    let urlImage = url.replace('https://bpresentacion.s3.us-west-2.amazonaws.com/orders/','');
    let name = 'ME-Comprobante' + this.order.id + '_' + urlImage;
    
    if (this.platform.is('ios')) {
      this.storageDirectory = this.file.documentsDirectory;
    }
    else if(this.platform.is('android')) {
      this.storageDirectory = this.file.dataDirectory;
    }

    const fileTransfer: FileTransferObject = this.transfer.create();    
    fileTransfer.download(url, this.storageDirectory + name).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      this.confirmation('Descarga Completa');
    }, (error) => {
      // handle error
    });
  }

  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any, title:any) {
    //this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    if (this.platform.is('android')) {
      this.iab.create('http://docs.google.com/gview?url=' + urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#003D6E');      
    } else {
      this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    }
  }
}
