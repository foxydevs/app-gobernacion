import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdersClientPage } from './orders-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    OrdersClientPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdersClientPage),
    PipeModule
  ],
  exports: [
    OrdersClientPage
  ]
})
export class OrdersClientPageModule {}