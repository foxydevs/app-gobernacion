import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { UbicationModalClientPage } from './ubication-modal-client';
 
@NgModule({
  declarations: [
    UbicationModalClientPage,
  ],
  imports: [
    IonicPageModule.forChild(UbicationModalClientPage),
    PipeModule
  ],
  exports: [
    UbicationModalClientPage
  ]
})
export class UbicationModalClientPageModule {}