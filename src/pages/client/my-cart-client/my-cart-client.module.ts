import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyCartClientPage } from './my-cart-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    MyCartClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MyCartClientPage),
    PipeModule
  ],
  exports: [
    MyCartClientPage
  ]
})
export class MyCartClientPageModule {}